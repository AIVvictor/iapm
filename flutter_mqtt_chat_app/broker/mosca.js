var mosca = require('mosca')
var log4js = require('log4js');
var jwt = require('jsonwebtoken');

log4js.configure({
  appenders: {
    cheese: {
      type: 'file',
      filename: 'myapp.log',
      maxLogSize: 10485760, // 10MB
      backups: 90,
      comporess: true
    }
  },
  categories: {
    default: { appenders: ['cheese'], level: 'debug' }
  }
})

var logger = log4js.getLogger('cheese');
logger.level = 'debug';

var moscaSettings = {
  port: 1883,
  persistence: {
    factory: mosca.persistence.Memory
  },
};

var server = new mosca.Server(moscaSettings);

server.on('ready', setup);

server.on('clientConnected', function (client) {
  logger.debug('client connected ', client.id);
});

server.on('clientDisconnected', function (client) {
  logger.debug('client disconnected ', client.id);
});

server.authenticate = function (client, username, password, callback) {
  try {
    var decoded = jwt.verify(password.toString(), 'secretKey');

    if (username == decoded.userId) {
      callback(null, true);
      console.log('authentication success')
    } else {
      console.log('authentication fail')
      callback(null, false);
    }
  } catch{ console.log('error'); }
}

server.published = function (packet, client, cb) {
  try {
    var decoded = JSON.parse(packet.payload);

    if (decoded.text != undefined) {
      var utf8Decoded = '';
      for (var i = 0; i < decoded.text.length; i++) {
        utf8Decoded += '%' + decoded.text[i].toString(16);
      }
      decoded.text = decodeURIComponent(utf8Decoded);
    }

    if (packet.topic.indexOf('echo') === 0) {
      logger.debug('ON PUBLISHED', decoded, 'on topic', packet.topic);
      return cb();
    }

    var newPacket = {
      topic: 'echo/' + packet.topic,
      payload: packet.payload.toString(),
      messageId: packet.messageId,
      qos: packet.qos,
      retain: packet.retain,
    };

    var newPacketForLogging = {
      topic: 'echo/' + packet.topic,
      payload: decoded,
      messageId: packet.messageId,
      qos: packet.qos,
      retain: packet.retain,
    };

    logger.debug('newPacket', newPacketForLogging);
    console.log('newPacketForLogging', newPacketForLogging);

    server.publish(newPacket, cb);
  } catch{ }
};

// fired when the mqtt server is ready
function setup() {
  logger.debug('Mosca server is up and running')
}