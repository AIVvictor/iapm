var aedes = require('aedes')();
var server = require('net').createServer(aedes.handle);
var port = 1883;
var jwt = require('jsonwebtoken');
var aedesPersistence = require('aedes-persistence')();
const { logger } = require('./config/logger');

server.listen(port, function () {
  // logger.info('server listening on port', port)
});

aedes.authenticate = function (client, username, password, callback) {
  try {
    var decoded = jwt.verify(password.toString(), 'secretKey');

    if (username == decoded.userId) {
      callback(null, true);
      logger.log('info', 'authentication success');
    } else {
      logger.log('info', 'authentication fail');
      callback(null, false);
    }
  } catch{ }
}

aedes.published = function (packet, client, callback) {
  try {
    if (packet.topic.indexOf('echo') === 0) {
      // logger.info('ON PUBLISHED' + decoded + 'on topic' + packet.topic);
      return callback();
    }

    aedesPersistence.storeRetained(packet);

    var decoded = JSON.parse(packet.payload);

    if (decoded.text != undefined) {
      var utf8Decoded = '';
      for (var i = 0; i < decoded.text.length; i++) {
        utf8Decoded += '%' + decoded.text[i].toString(16);
      }
      decoded.text = decodeURIComponent(utf8Decoded);
    }

    var newPacket = {
      cmd: packet.cmd,
      qos: packet.qos,
      topic: 'echo/' + packet.topic,
      payload: packet.payload.toString(),
      retain: packet.retain,
    };

    var newPacketForLogging = {
      cmd: packet.cmd,
      qos: packet.qos,
      topic: packet.topic,
      payload: decodedPacket,
      retain: packet.retain,
    };

    logger.log('info', newPacketForLogging);

    aedes.publish(newPacket);
  } catch { };
}

aedes.on('client', function (client) {
  logger.log('info', 'client');
})

aedes.on('client', function (client) {
  logger.log('info', 'clientReady');
})

aedes.on('clientDisconnect', function (client) {
  logger.log('info', 'clientDisconnect');
})

aedes.on('clientError', function (client, err) {
  logger.log('info', 'clientError')
})

aedes.on('keepaliveTimeout', function (client) {
  logger.log('info', 'keepaliveTimeout')
})

aedes.on('publish', function (packet, client) {
  logger.log('info', 'publish')
})

aedes.on('ack', function (packet, client) {
  logger.log('info', 'ack')
})

aedes.on('ping', function (packet, client) {
  logger.log('info', 'ping')
})

aedes.on('subscribe', function (subscriptions, client) {
  logger.log('info', 'SUBSCRIBED')
})

aedes.on('unsubscribe', function (subscriptions, client) {
  logger.log('info', 'UNSUBSCRIBED');
})

aedes.on('connackSent', function (packet, client) {
  logger.log('info', 'connackSent');
})

aedes.on('closed', function () {
  logger.log('info', 'closed');
})

