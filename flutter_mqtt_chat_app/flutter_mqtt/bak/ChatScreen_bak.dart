//import 'dart:async';
//import 'dart:convert';
//import 'dart:core';
//
//import 'package:flutter/material.dart';
//import 'package:mqtt_client/mqtt_client.dart';
//import 'package:random_string/random_string.dart';
//import 'package:flutter_secure_storage/flutter_secure_storage.dart';
//
//import '../lib/CommonController.dart';
//import '../lib/Themes.dart';
//
//class ChatScreen extends StatefulWidget {
//  ChatScreen({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  ChatScreenState createState() => ChatScreenState();
//}
//
//class ChatScreenState extends State<ChatScreen> {
//  bool _switchVal = true;
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(title: Text('MQTT Flutter Chat App')),
//        body: Column(children: <Widget>[
//          Container(
//            child: Row(children: <Widget>[
//              Container(
//                width: 180,
//                padding: EdgeInsets.only(top: 16, left: 16, right: 16),
//                child: TextField(
//                    enabled: client.connectionStatus.state == MqttConnectionState.disconnected ? true : false,
//                    controller: hostTextController,
//                    style: MyTheme.kTextStyle,
//                    textAlign: TextAlign.center,
//                    decoration: InputDecoration(
//                      labelText: 'IP',
//                      border: OutlineInputBorder(
//                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                      ),
//                    )),
//              ),
//              Flexible(
////                margin: const EdgeInsets.symmetric(vertical: 8.0),
////                decoration: BoxDecoration(color: Theme.of(context).cardColor),
//                  child: FlatButton(
//                      child: Text(connectTextController.text, style: MyTheme.kTextStyle), onPressed: () => _connect()))
//            ], mainAxisAlignment: MainAxisAlignment.start),
//          ),
//          Container(
//            child: Row(children: <Widget>[
//              Container(
//                width: 180,
////                height: 60,
//                padding: EdgeInsets.only(top: 16, left: 16, right: 16),
//                child: TextField(
//                    controller: subTopicTextController,
//                    enabled: subTextController.text == 'SUB' ? true : false,
//                    style: MyTheme.kTextStyle,
//                    textAlign: TextAlign.center,
//                    decoration: InputDecoration(
//                      labelText: 'Sub Topic',
//                      border: OutlineInputBorder(
//                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                      ),
//                    )),
//              ),
//              Container(
//                  child: FlatButton(
//                      child: Text(subTextController.text, style: MyTheme.kTextStyle),
//                      onPressed: () => _subscribe(subTextController.text)),
//                  width: 100),
//              Container(
//                child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
//                  Container(
//                    child: Text('retain'),
////              width: 200,
////                      padding: EdgeInsets.only(left: 50),
//                  ),
//                  Container(
//                    child: Switch(
//                      onChanged: (bool value) {
//                        setState(() => this._switchVal = value);
//                      },
//                      value: this._switchVal,
//                    ),
////              width: 200,
////                      padding: EdgeInsets.only(left: 8, right: 16),
//                  )
//                ]),
//                width: 100,),
//            ], mainAxisAlignment: MainAxisAlignment.start),
//          ),
//          Container(
//              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
//                Container(
//                  child: TextField(
//                      controller: pubTopicTextController,
//                      style: MyTheme.kTextStyle,
//                      textAlign: TextAlign.center,
//                      decoration: InputDecoration(
//                        labelText: 'Pub Topic',
//                        border: OutlineInputBorder(
//                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                        ),
//                      )),
//                  width: 200,
//                  padding: EdgeInsets.only(top: 16, left: 16, right: 16),
//                ),
//                Container(
//                  child: TextField(
//                      enabled: false,
//                      controller: clientIdTextController,
//                      style: MyTheme.kTextStyle,
//                      textAlign: TextAlign.center,
//                      decoration: InputDecoration(
//                        labelText: 'Client Identifier',
//                        border: OutlineInputBorder(
//                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
//                        ),
//                      )),
//                  width: 200,
//                  padding: EdgeInsets.only(top: 16, left: 16, right: 16),
//                )
//              ])),
//
//          Flexible(
//            child: ListView.builder(
//              padding: EdgeInsets.all(8.0),
//              reverse: true,
//              itemBuilder: (_, int index) => messages[index],
//              itemCount: messages.length,
//            ),
//          ),
//          Divider(height: 1.0),
//          Container(
//            decoration: BoxDecoration(color: Theme.of(context).cardColor),
//            child: _buildTextComposer(),
//          )
//        ]));
//  }
//
//  Widget _buildTextComposer() {
//    return IconTheme(
//        data: IconThemeData(color: Theme.of(context).accentColor),
//        child: Container(
//            margin: const EdgeInsets.symmetric(horizontal: 8.0),
//            child: Row(children: <Widget>[
//              Flexible(
//                child: TextField(
//                    controller: messageTextController,
//                    onSubmitted: _handleSubmitted,
//                    decoration: InputDecoration.collapsed(hintText: "Send a message")),
//              ),
//              Container(
//                  margin: const EdgeInsets.symmetric(horizontal: 4.0),
//                  child:
//                  IconButton(icon: Icon(Icons.send), onPressed: () => _handleSubmitted(messageTextController.text)))
//            ])));
//  }
//
//  void _handleSubmitted(String text) {
//    if (client.connectionStatus.state == MqttConnectionState.connected &&
//        text.isEmpty == false &&
//        pubTopicTextController.text.isEmpty == false) {
//      ChatMessage message = ChatMessage(id: clientIdTextController.text, text: text, isMyMessage: true);
//      setState(() {
//        messages.insert(0, message);
//      });
//
//      final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
//
//      final encodedText = utf8.encode(text);
//
////      print('encodedText: $encodedText');
//
//      builder.addString(json.encode({'clientId': clientIdTextController.text, 'text': encodedText}));
//
//      /// Publish it
////      print('EXAMPLE::Publishing our topic');
//
////      print('Send Message: ${builder.payload}');
//
//      client.publishMessage(pubTopicTextController.text, MqttQos.atMostOnce, builder.payload, retain: true);
//      print(pubTopicTextController.text);
//      messageTextController.clear();
//    }
//  }
//
//  Future<String> _getClientId() async {
////    final response = await http.get('http://192.168.0.118:3000/getClientId');
////    _clientId = jsonDecode(response.body)['ID'];
////    print(response.body);
//
//    clientId = randomAlphaNumeric(10);
//
//    return clientId;
//  }
//
//  Future<int> _connect() async {
//    if (client.connectionStatus.state == MqttConnectionState.connected) {
//      client.disconnect();
//
//      return 0;
//    }
//
//    messages.clear();
//
//    client.server = hostTextController.text;
//
//    client.logging(on: false);
//
//    client.keepAlivePeriod = 20;
//
//    client.onDisconnected = onDisconnected;
//
//    client.onConnected = onConnected;
//
//    client.onSubscribed = onSubscribed;
//
//    client.pongCallback = pong;
//
//    final MqttConnectMessage connMess = MqttConnectMessage()
//        .withClientIdentifier(await _getClientId())
//        .keepAliveFor(20) // Must agree with the keep alive set above or not set
//        .withWillTopic('willtopic') // If you set this you must set a will message
//        .withWillMessage('My Will message')
//        .startClean() // Non persistent session for testing
//        .withWillQos(MqttQos.atMostOnce);
////    print('EXAMPLE::Mosquitto client connecting....');
//    client.connectionMessage = connMess;
//
//    // Create storage
//    final storage = new FlutterSecureStorage();
//
//    String userId = await storage.read(key: 'userId');
//    String token = await storage.read(key: 'token');
//
//    try {
//      await client.connect(userId, token);
//    } on Exception catch (e) {
//      print('EXAMPLE::client exception - $e');
//      client.disconnect();
//    }
//
//    /// Check we are connected
//    if (client.connectionStatus.state == MqttConnectionState.connected) {
////      print('EXAMPLE::Mosquitto client connected');
//    } else {
//      /// Use status here rather than state if you also want the broker return code.
////      print(
////          'EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, status is ${client.connectionStatus}');
//      client.disconnect();
////      exit(-1);
//    }
//
//    client.published.listen((MqttPublishMessage message) {
////      print(
////          'EXAMPLE::Published notification:: topic is ${message.variableHeader.topicName}, with Qos ${message.header.qos}');
//    });
//
//    return 0;
//  }
//
//  Future<int> _subscribe(String subText) async {
//    if (client.connectionStatus.state != MqttConnectionState.connected) return 0;
//
//    switch (subText) {
//      case 'SUB':
////        print('EXAMPLE::Subscribing to the ' +
////            _subTopicTextController.text +
////            ' topic');
//        client.subscribe(subTopicTextController.text, MqttQos.atMostOnce);
//        setState(() {
//          subTextController.text = 'UNSUB';
//        });
//        break;
//      case 'UNSUB':
////        print('EXAMPLE::Unsubscribing to the ' +
////            _subTopicTextController.text +
////            ' topic');
//        client.unsubscribe(subTopicTextController.text);
//        setState(() {
//          subTextController.text = 'SUB';
//        });
//        break;
//      default:
//        break;
//    }
//
//    return 0;
//  }
//
//  /// The successful connect callback
//  void onConnected() {
////    print(
////        'EXAMPLE::OnConnected client callback - Client connection was sucessful');
//    setState(() {
//      connectTextController.text = 'DISCONNECT';
//      clientIdTextController.text = clientId;
//    });
//  }
//
//  /// The unsolicited disconnect callback
//  void onDisconnected() {
////    print('EXAMPLE::OnDisconnected client callback - Client disconnection');
//
//    if (client.connectionStatus.returnCode == MqttConnectReturnCode.solicited) {
////      print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
//    }
//
//    setState(() {
//      connectTextController.text = 'CONNECT';
//      subTextController.text = 'SUB';
//    });
//
////    _deleteClientId();
//
//    clientIdTextController.clear();
//  }
//
//  /// The subscribed callback
//  void onSubscribed(String topic) {
////    print('EXAMPLE::Subscription confirmed for topic $topic');
//  }
//
//  /// Pong callback
//  void pong() {
////    print('EXAMPLE::Ping response client callback invoked');
//  }
//}
//
//class ChatMessage extends StatelessWidget {
//  final String id;
//  final String text;
//  final bool isMyMessage;
//
//  ChatMessage({this.id, this.text, this.isMyMessage});
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 8),
//        child: isMyMessage
//            ? Wrap(
//                spacing: 8.0,
//                runSpacing: 4.0,
//                direction: Axis.horizontal,
//                alignment: WrapAlignment.end,
//                children: <Widget>[
//                    Chip(label: Column(children: <Widget>[Container(child: Text(text))]))
//                  ])
//            : Wrap(
//                spacing: 8.0,
//                runSpacing: 4.0,
//                direction: Axis.horizontal,
//                alignment: WrapAlignment.start,
//                children: <Widget>[
//                    Chip(
//                        avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text(id[0])),
//                        label: Column(children: <Widget>[
//                          Text(id, style: Theme.of(context).textTheme.subtitle1),
//                          Container(margin: EdgeInsets.only(top: 5.0, right: 8), child: Text(text))
//                        ]))
//                  ]));
//  }
//}
//
////Future<String> _getClientId() async {
////    final response = await http.get('http://192.168.0.118:3000/getClientId');
////    _clientId = jsonDecode(response.body)['ID'];
////    print(response.body);
//
////  _clientId = randomAlphaNumeric(10);
////
////  return _clientId;
////}
//
////
////  Future<void> _deleteClientId() async {
////    await http.post('http://192.168.0.118:3000/deleteClientId',
////        body: {'clientId': data.clientIdTextController.text});
////  }
