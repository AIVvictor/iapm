import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:random_string/random_string.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'CommonController.dart';
import 'Themes.dart';

class ConnectScreen extends StatefulWidget {
  ConnectScreen({Key key}) : super(key: key);

  @override
  ConnectScreenState createState() => ConnectScreenState();
}

class ConnectScreenState extends State<ConnectScreen> with AutomaticKeepAliveClientMixin{
  String clientId = '';

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('MQTT Flutter Chat App')),
        body: Column(children: <Widget>[
          Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Container(
              child: TextField(
                  enabled: false,
                  controller: clientIdTextController,
                  style: MyTheme.kTextStyle,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    labelText: 'Client Identifier',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  )),
              width: 360,
              margin: EdgeInsets.only(top: 16, left: 16, right: 16),
            )
          ])),
          Container(
            child: Row(children: <Widget>[
              Container(
                width: 180,
                margin: EdgeInsets.only(top: 16, left: 16),
                child: TextField(
                    enabled: client.connectionStatus.state == MqttConnectionState.disconnected
                        ? true
                        : false,
                    controller: hostTextController,
                    style: MyTheme.kTextStyle,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      labelText: 'IP',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    )),
              ),
              Center(
                  child: Container(
//                margin: EdgeInsets.only(left: 16),
//                margin: const EdgeInsets.symmetric(vertical: 8.0),
//                decoration: BoxDecoration(color: Theme.of(context)
                child: FlatButton(
                    child: Text(connectTextController.text,
                        style: MyTheme.kTextStyle, textAlign: TextAlign.left),
                    onPressed: () => _connect()),
                width: 180,
              ))
            ], mainAxisAlignment: MainAxisAlignment.start),
          ),
          Container(
            child: Row(children: <Widget>[
              Container(
                width: 180,
//                height: 60,
                margin: EdgeInsets.only(top: 16, left: 16),
                child: TextField(
                    controller: subTopicTextController,
                    enabled: subTextController.text == 'SUB' ? true : false,
                    style: MyTheme.kTextStyle,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      labelText: 'Sub Topic',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    )),
              ),
              Center(
                  child: Container(
//                margin: EdgeInsets.only(left: 16),
                child: FlatButton(
                    child: Text(subTextController.text,
                        style: MyTheme.kTextStyle, textAlign: TextAlign.left),
                    onPressed: () => _subscribe(subTextController.text)),
                width: 180,
              ))
            ], mainAxisAlignment: MainAxisAlignment.start),
          ),
          Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Container(
              child: TextField(
                  controller: pubTopicTextController,
                  style: MyTheme.kTextStyle,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    labelText: 'Pub Topic',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  )),
              width: 180,
              margin: EdgeInsets.only(top: 16, left: 16),
            ),
            Center(
                child: Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                Container(
                  child: Text('retain', style: MyTheme.kTextStyle),
                  margin: EdgeInsets.only(top: 16, left: 30),
//                  width: 50,
//                      margin: EdgeInsets.only(left: 50),
                ),
                Container(
                    child: Switch(
                      onChanged: (bool value) {
                        print(value);
                        setState(() => switchVal = value);
                      },
                      value: switchVal,
                    ),
                    margin: EdgeInsets.only(top: 16, right: 30)
//              width: 200,
//                      margin: EdgeInsets.only(left: 8, right: 16),
                    )
              ]),
              width: 180,
            ))
          ]))
        ]));
  }

  Future<String> _getClientId() async {
//    final response = await http.get('http://192.168.0.118:3000/getClientId');
//    _clientId = jsonDecode(response.body)['ID'];
//    print(response.body);

    clientId = randomAlphaNumeric(10);

    return clientId;
  }

  Future<int> _connect() async {
    if (client.connectionStatus.state == MqttConnectionState.connected) {
      client.disconnect();

      return 0;
    }

    messages.clear();

    client.server = hostTextController.text;

    client.logging(on: false);

    client.keepAlivePeriod = 20;

    client.onDisconnected = onDisconnected;

    client.onConnected = onConnected;

    client.onSubscribed = onSubscribed;

    client.pongCallback = pong;

    final MqttConnectMessage connMess = MqttConnectMessage()
        .withClientIdentifier(await _getClientId())
        .keepAliveFor(20) // Must agree with the keep alive set above or not set
        .withWillTopic('willtopic') // If you set this you must set a will message
        .withWillMessage('My Will message')
        .startClean() // Non persistent session for testing
        .withWillQos(MqttQos.atMostOnce);
//    print('EXAMPLE::Mosquitto client connecting....');
    client.connectionMessage = connMess;

    // Create storage
    final storage = new FlutterSecureStorage();

    String userId = await storage.read(key: 'userId');
    String token = await storage.read(key: 'token');

    try {
      await client.connect(userId, token);
    } on Exception catch (e) {
      print('EXAMPLE::client exception - $e');
      client.disconnect();
    }

    client.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
      final MqttPublishMessage recMess = c[0].payload;
      final String pt = MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

      /// JSON parsing
      var _pt = json.decode(pt);

      /// 내가 보낸 메시지는 내가 받지 않도록 처리
      if (_pt['clientId'] != clientIdTextController.text) {
        /// JSON maps 이나 lists 는 정해진 generic type 이 없기 때문에 assign 전에 필요한 형으로 변환 필요
        List<int> _text = List<int>.from(_pt['text']);

        /// UTF-8 decoding
        String decodedText = utf8.decode(_text);

        message = ChatMessage(id: _pt['clientId'], text: decodedText, isMyMessage: false);
      }

//      print(
//          'EXAMPLE::Change notification:: topic is <${c[0].topic}>, payload is <-- $pt -->');
//      print('');
    });

    /// Check we are connected
    if (client.connectionStatus.state == MqttConnectionState.connected) {
//      print('EXAMPLE::Mosquitto client connected');
    } else {
      /// Use status here rather than state if you also want the broker return code.
//      print(
//          'EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, status is ${client.connectionStatus}');
      client.disconnect();
//      exit(-1);
    }

    client.published.listen((MqttPublishMessage message) {
//      print(
//          'EXAMPLE::Published notification:: topic is ${message.variableHeader.topicName}, with Qos ${message.header.qos}');
    });

    return 0;
  }

  Future<int> _subscribe(String subText) async {
    if (client.connectionStatus.state != MqttConnectionState.connected) return 0;

    switch (subText) {
      case 'SUB':
//        print('EXAMPLE::Subscribing to the ' +
//            _subTopicTextController.text +
//            ' topic');
        client.subscribe(subTopicTextController.text, MqttQos.atMostOnce);
        setState(() {
          subTextController.text = 'UNSUB';
        });
        break;
      case 'UNSUB':
//        print('EXAMPLE::Unsubscribing to the ' +
//            _subTopicTextController.text +
//            ' topic');
        client.unsubscribe(subTopicTextController.text);
        setState(() {
          subTextController.text = 'SUB';
        });
        break;
      default:
        break;
    }

    return 0;
  }

  /// The successful connect callback
  void onConnected() {
//    print(
//        'EXAMPLE::OnConnected client callback - Client connection was sucessful');
    setState(() {
      connectTextController.text = 'DISCONNECT';
      clientIdTextController.text = clientId;
    });
  }

  /// The unsolicited disconnect callback
  void onDisconnected() {
//    print('EXAMPLE::OnDisconnected client callback - Client disconnection');

    if (client.connectionStatus.returnCode == MqttConnectReturnCode.solicited) {
//      print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
    }

    setState(() {
      connectTextController.text = 'CONNECT';
      subTextController.text = 'SUB';
    });

//    _deleteClientId();

    clientIdTextController.clear();
  }

  /// The subscribed callback
  void onSubscribed(String topic) {
//    print('EXAMPLE::Subscription confirmed for topic $topic');
  }

  /// Pong callback
  void pong() {
//    print('EXAMPLE::Ping response client callback invoked');
  }
}
