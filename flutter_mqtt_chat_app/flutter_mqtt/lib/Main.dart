import 'package:flutter/material.dart';
import 'dart:core';

import 'LoginScreen.dart';
import 'ConnectScreen.dart';
import 'ChatScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'MQTT Flutter Chat App',
        theme: ThemeData(primarySwatch: Colors.blue),
        routes: <String, WidgetBuilder>{'/': (context) => LoginScreen(), '/HomeScreen': (context) => HomeScreen()});
  }
}

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  PageController _pageController;
  int _page = 0;
String routes;
  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

//  @override
//  void dispose() {
//    super.dispose();
//    _pageController.dispose();
//  }

  void navigationTapped(int page) {
    // Animating to the page.
    // You can use whatever duration and curve you like
    _pageController.animateToPage(page, duration: const Duration(milliseconds: 10), curve: Curves.linear);
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new PageView(
        children: [ConnectScreen(), ChatScreen()],
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          canvasColor: Colors.white,
        ), // sets the inactive color of the `BottomNavigationBar`
        child: new BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          fixedColor: const Color(0xFF2845E7),
          items: [
            new BottomNavigationBarItem(
              icon: new Icon(
                Icons.swap_horiz,
              ),
              title: SizedBox.shrink(),
            ),
            new BottomNavigationBarItem(
                icon: new Icon(
                  Icons.chat,
                ),
                title: SizedBox.shrink())
          ],
          onTap: navigationTapped,
          currentIndex: _page,
        ),
      ),
    );
  }
}