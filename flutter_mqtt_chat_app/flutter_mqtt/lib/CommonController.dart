import 'package:flutter/material.dart';
import 'package:mqtt_client/mqtt_client.dart';

final clientIdTextController = TextEditingController();
final hostTextController = TextEditingController(text: '192.168.0.118');
final connectTextController = TextEditingController(text: 'CONNECT');
final subTopicTextController = TextEditingController(text: 'chat room');
final pubTopicTextController = TextEditingController(text: 'chat room');
final subTextController = TextEditingController(text: 'SUB');
final messageTextController = TextEditingController();
final List<ChatMessage> messages = <ChatMessage>[];
final MqttClient client = MqttClient('192.168.0.118', '');
ChatMessage message = ChatMessage();
bool switchVal = false;

class ChatMessage extends StatelessWidget {
  final String id;
  final String text;
  final bool isMyMessage;

  ChatMessage({this.id, this.text, this.isMyMessage});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 8),
        child: isMyMessage
            ? Wrap(
            spacing: 8.0,
            runSpacing: 4.0,
            direction: Axis.horizontal,
            alignment: WrapAlignment.end,
            children: <Widget>[
              Chip(label: Column(children: <Widget>[Container(child: Text(text))]))
            ])
            : Wrap(
            spacing: 8.0,
            runSpacing: 4.0,
            direction: Axis.horizontal,
            alignment: WrapAlignment.start,
            children: <Widget>[
              Chip(
                  avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text(id[0])),
                  label: Column(children: <Widget>[
                    Text(id, style: Theme.of(context).textTheme.subtitle1),
                    Container(margin: EdgeInsets.only(top: 5.0, right: 8), child: Text(text))
                  ]))
            ]));
  }
}