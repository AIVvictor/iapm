import 'dart:convert';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:mqtt_client/mqtt_client.dart';

import 'CommonController.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({Key key}) : super(key: key);

  @override
  ChatScreenState createState() => ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> with AutomaticKeepAliveClientMixin{
  @override
  bool get wantKeepAlive => true;

  @override
  initState() {
    super.initState();

    try {
      client.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
        final MqttPublishMessage recMess = c[0].payload;
        final String pt = MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

        /// JSON parsing
        var _pt = json.decode(pt);

        /// 내가 보낸 메시지는 내가 받지 않도록 처리
        if (_pt['clientId'] != clientIdTextController.text) {
          /// JSON maps 이나 lists 는 정해진 generic type 이 없기 때문에 assign 전에 필요한 형으로 변환 필요
          List<int> _text = List<int>.from(_pt['text']);

          /// UTF-8 decoding
          String decodedText = utf8.decode(_text);

          ChatMessage message = ChatMessage(id: _pt['clientId'], text: decodedText, isMyMessage: false);

          setState(() {
            messages.insert(0, message);
          });
        }

//      print(
//          'EXAMPLE::Change notification:: topic is <${c[0].topic}>, payload is <-- $pt -->');
//      print('');
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('MQTT Flutter Chat App')),
        body: Column(children: <Widget>[
          Flexible(
            child: ListView.builder(
              padding: EdgeInsets.all(8.0),
              reverse: true,
              itemBuilder: (_, int index) => messages[index],
              itemCount: messages.length,
            ),
          ),
          Divider(height: 1.0),
          Container(
            decoration: BoxDecoration(color: Theme.of(context).cardColor),
            child: _buildTextComposer(),
          )
        ]));
  }

  Widget _buildTextComposer() {
    return IconTheme(
        data: IconThemeData(color: Theme.of(context).accentColor),
        child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(children: <Widget>[
              Flexible(
                child: TextField(
                    controller: messageTextController,
                    onSubmitted: _handleSubmitted,
                    decoration: InputDecoration.collapsed(hintText: "Send a message")),
              ),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 4.0),
                  child:
                      IconButton(icon: Icon(Icons.send), onPressed: () => _handleSubmitted(messageTextController.text)))
            ])));
  }

  void _handleSubmitted(String text) {
    if (client.connectionStatus.state == MqttConnectionState.connected &&
        text.isEmpty == false &&
        pubTopicTextController.text.isEmpty == false) {
      ChatMessage message = ChatMessage(id: clientIdTextController.text, text: text, isMyMessage: true);
      setState(() {
        messages.insert(0, message);
      });

      final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();

      final encodedText = utf8.encode(text);

//      print('encodedText: $encodedText');

      builder.addString(json.encode({'clientId': clientIdTextController.text, 'text': encodedText}));

      /// Publish it
//      print('EXAMPLE::Publishing our topic');

//      print('Send Message: ${builder.payload}');

      client.publishMessage(pubTopicTextController.text, MqttQos.atMostOnce, builder.payload, retain: switchVal);
      messageTextController.clear();
    }
  }
}
