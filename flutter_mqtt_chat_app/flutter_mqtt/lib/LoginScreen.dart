import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

// Used for controlling whether the user is loggin or creating an account
enum FormType { login, register }

class _LoginPageState extends State<LoginScreen> {
  final TextEditingController _idFilter = TextEditingController();
  final TextEditingController _passwordFilter = TextEditingController();
  String _id = "";
  String _password = "";
  FormType _form = FormType
      .login; // our default setting is to login, and we should switch to creating an account when the user chooses to

  _LoginPageState() {
    _idFilter.addListener(_idListen);
    _passwordFilter.addListener(_passwordListen);
  }

  void _idListen() {
    if (_idFilter.text.isEmpty) {
      _id = "";
    } else {
      _id = _idFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  // Swap in between our two forms, registering and logging in
  void _formChange() async {
    setState(() {
      if (_form == FormType.register) {
        _form = FormType.login;
      } else {
        _form = FormType.register;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            _buildTextFields(),
            _buildButtons(),
          ],
        ),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return AppBar(
      title: Text("Simple Login Example"),
      centerTitle: true,
    );
  }

  Widget _buildTextFields() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            child: TextField(
              controller: _idFilter,
              decoration: InputDecoration(labelText: 'ID'),
            ),
          ),
          Container(
            child: TextField(
              controller: _passwordFilter,
              decoration: InputDecoration(labelText: 'Password'),
              obscureText: true,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButtons() {
    if (_form == FormType.login) {
      return Container(
        margin: const EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          children: <Widget>[
            RaisedButton(child: Text('Login'), onPressed: _loginPressed
//    Navigator.pushNamed(context, '/ChatScreen');
            ),
//            FlatButton(
//              child: Text('Dont have an account? Tap here to register.'),
//              onPressed: _formChange,
//            ),
//            FlatButton(
//              child: Text('Forgot Password?'),
//              onPressed: _passwordReset,
//            )
          ],
        ),
      );
    } else {
      return Container(
        child: Column(
          children: <Widget>[
            RaisedButton(
              child: Text('Create an Account'),
              onPressed: _createAccountPressed,
            ),
            FlatButton(
              child: Text('Have an account? Click here to login.'),
              onPressed: _formChange,
            )
          ],
        ),
      );
    }
  }

  Future<Post> createPost(String url, {Map body}) async {
    return http.post(url, body: body).then((http.Response response) {
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw Exception("Error while fetching data");
      }

      return Post.fromJson(json.decode(response.body));
    });
  }

  // These functions can self contain any user auth logic required, they all have access to _id and _password

  void _loginPressed() async {
    final storage = new FlutterSecureStorage();

    Post newPost = Post(userId: _id, password: _password, token: '');
    Post recv = await createPost('http://192.168.0.118:3000/login', body: newPost.toMap());

    await storage.write(key: 'userId', value: _id);
    await storage.write(key: 'token', value: recv.token);

    Navigator.pushNamed(context, '/HomeScreen');
  }

  void _createAccountPressed() {
    print('The user wants to create an accoutn with $_id and $_password');
  }

  void _passwordReset() {
    print("The user wants a password reset request sent to $_id");
  }
}

class Post {
  final String userId;
  final String password;
  final String token;

  Post({this.userId, this.password, this.token});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(userId: json['userId'], password: json['password'], token: json['token']);
  }

  Map toMap() {
    var map = Map<String, dynamic>();
    map["userId"] = userId;
    map["password"] = password;
    map['token'] = token;

    return map;
  }
}
