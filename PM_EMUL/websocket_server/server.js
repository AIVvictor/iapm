const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

wss.addListener('connection', function connection(ws) {
  OnMessage(ws);
  OnPong(ws);

  ws.ping();
});

function OnMessage(ws) {
  ws.addListener('message', function incoming(message) {
    console.log('received: %s', message);
    ws.send('echo: ' + message);
  });
}

function OnPong(ws) {
  ws.addListener('pong', function () {
    try {
      console.log('[' + new Date() + '] pong');
      clearTimeout(setTimeout(function () { ws.ping(); }, 5000));
    } catch{
      ws.close(function () {
      });
    }
  })
}