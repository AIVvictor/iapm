﻿namespace PM_EMUL
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.CheckBox();
            this.btnT1PMStart = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnConnect.FlatAppearance.CheckedBackColor = System.Drawing.Color.Yellow;
            this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnect.Location = new System.Drawing.Point(44, 37);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(153, 169);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "CONNECT";
            this.btnConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.CheckedChanged += new System.EventHandler(this.btnConnect_CheckedChanged);
            // 
            // btnT1PMStart
            // 
            this.btnT1PMStart.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnT1PMStart.FlatAppearance.CheckedBackColor = System.Drawing.Color.Yellow;
            this.btnT1PMStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnT1PMStart.Location = new System.Drawing.Point(235, 37);
            this.btnT1PMStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnT1PMStart.Name = "btnT1PMStart";
            this.btnT1PMStart.Size = new System.Drawing.Size(178, 61);
            this.btnT1PMStart.TabIndex = 3;
            this.btnT1PMStart.Text = "입국장 차량 주행 시작";
            this.btnT1PMStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnT1PMStart.UseVisualStyleBackColor = true;
            this.btnT1PMStart.CheckedChanged += new System.EventHandler(this.btnT1PMStart_CheckedChanged);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 244);
            this.Controls.Add(this.btnT1PMStart);
            this.Controls.Add(this.btnConnect);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.CheckBox btnConnect;
        private System.Windows.Forms.CheckBox btnT1PMStart;
    }
}