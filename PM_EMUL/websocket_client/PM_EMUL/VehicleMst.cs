﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;

namespace PM_EMUL
{
    class Vehicle
    {
        string vehicleId = "";
        string password = "";
        string authenticationKey = "";

        public string VehicleId { get => vehicleId; set => vehicleId = value; }
        public string Password { get => password; set => password = value; }
        public string AuthenticationKey { get => authenticationKey; set => authenticationKey = value; }

        public IRestResponse Authentication(string vehicleId, string password, string authenticaionKey)
        {
            var client = new RestClient("");
            var request = new RestRequest("/v1/authen/" + vehicleId + "/" + password + "/" + authenticationKey, Method.GET);
            var response = client.ExecuteAsync(request);
            return response.Result;
        }
    }
}
