﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using WebSocketSharp;
using log4net;
using log4net.Config;

namespace PM_EMUL
{
    public partial class MainWindow : Form
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(typeof(MainWindow));

        string url = "ws://" + GlobalVariable.host + ":" + GlobalVariable.port;
        WebSocket ws;

        public MainWindow()
        {
            InitializeComponent();
        }

        void WsOnOpen(object sender, EventArgs e)
        {
            ws.Send("client connection message");
        }

        void WsOnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsPing)
            {
                ws.Send("pong");
            }
            else
            {
                Console.WriteLine(e.Data);
            };


        }

        void WsOnError(object sender, WebSocketSharp.ErrorEventArgs e)
        {
            Console.WriteLine(e.Message);
        }

        void WsOnClose(object sender, CloseEventArgs e)
        {
            if (btnConnect.Checked)
            {
                Thread.Sleep(1000);
                Console.WriteLine(e.Reason);
                Console.WriteLine("RECONNECTING");
                ws.Connect();
            }
            else
            {
                Console.WriteLine("disconnection by client");
            }
        }

        private void btnConnect_CheckedChanged(object _sender, EventArgs _e)
        {
            if (btnConnect.Checked)
            {
                ws = new WebSocket(url);

                // ws.Log.Level = LogLevel.Trace;

                // ping을 받았을 때 pong 전송
                // ws.EmitOnPing = true;

                ws.OnOpen += new EventHandler(WsOnOpen);
                ws.OnMessage += new EventHandler<MessageEventArgs>(WsOnMessage);
                ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(WsOnError);
                ws.OnClose += new EventHandler<CloseEventArgs>(WsOnClose);

                ws.Connect();

                btnConnect.Text = "CONNECTING";
            }
            else
            {
                ws.Close();

                btnConnect.Text = "CONNECT";
            }
        }

        private void btnT1PMStart_CheckedChanged(object sender, EventArgs e)
        {
            if (btnT1PMStart.Checked)
            {
                // 인증
                string vehicleId = "";
                string password = "";
                string authenticationKey = "";

                // 주기적으로 콜 요청
                // if 다음콜이 있는 경우
                // else if p1콜이 있는 경우
                // else if 다음콜이 유효시간 내에 없는 경우
                Vehicle v = new Vehicle();
                var token = v.Authentication(vehicleId, password, authenticationKey);
            }
            else
            {

            }
        }
    }
}